---
title: Contribute
layout: doc
menu:
  main:
    weight: 5
---

Interested in making _ghostwriter_ even better? Please read the sections below to see how you can contribute.

## Bugs Reports

Found a bug? Please report it on the [KDE issue tracker](https://bugs.kde.org). Don't forget to follow the [issue reporting guidelines](https://community.kde.org/Get_Involved/Issue_Reporting)!  Most importantly, don't forget to include the version number of _ghostwriter_ that you are using.

**Warning**: Failing to provide the information required in the bug report template will result in the bug report being automatically closed.

## Bug Triaging

You can help triage issues in the [issue tracker][KDE_Bugs] by reproducing bugs and getting more information from bug reporters to help narrow down where the problem is. Closing duplicate issues and other tidying would also be appreciated. For more information, please see the [bug triaging guidelines](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging).

## New Features and Bug Fixes

Do you know how to code? Follow these steps:

[KDE_Bugs]: https://bugs.kde.org
[KDE_StyleGuide]: https://community.kde.org/Policies/Frameworks_Coding_Style
[KDE_Merge]: https://invent.kde.org/office/ghostwriter/-/merge_requests

1. Submit an issue for your feature or bug on the [KDE issue tracker][KDE_Bugs].  
**Note:** This step is necessary to ensure that the new feature meets the project goals or that the bug isn't really a feature. You will also want to coordinate what areas of code you will be updating to avoid merge conflicts in case someone else is working on that same area of code as well.
2. After discussion for your new issue, code your feature or bug fix. Please follow the [KDE Frameworks coding style guide][KDE_StyleGuide].
**Important**: *Please keep your code focused on the issue at hand.* Unrelated changes to the code belong to separate issues/merge requests to make code review and testing easier.
3. Submit a pull request against the master branch with your code updates.  
**Important**: Please squash your commits in git before submitting the pull request!
4. Wait for a brief code review and at least two other volunteers to test your feature or bug fix.
5. Make updates to your pull request as you receive feedback.
6. Once your pull request is approved by at least two other volunteer testers, your pull request will be merged.

## Code Reviews

Do you know how to code and have an eye for detail? Volunteer to peer review new merge requests!

1. Look for a [new merge request](https://invent.kde.org/office/ghostwriter/-/merge_requests), and add a comment that you are volunteering to code review it.
2. Submit code review comments.
3. Wait for updates to the merge request (if needed).
4. Continue providing feedback until you are confident the code is sound.
5. Leave a comment with your approval of the merge request.
6. Once at least two volunteer testers submit their approval, the merge request will be merged.

Here are some tips of what to look for in code reviews:

* Does the submitted code follow the [KDE Frameworks coding style guide][KDE_StyleGuide]?
* Is the code readable, with comments explaining any non-obvious lines?
* Is the code "pessimistic"? In other words, does it check for invalid values before using them and handle possible error conditions?
* Are multiple comparisons or statements wrapped in parentheses within `if` statements to prevent accidents with order of operations?  Examples:
    ```cpp
    // Wrong
    if (a == b || c > d);

    // Correct
    if ((a == b) || (c > d));
    ```
* Are literals on the left side of the `==` operator to prevent a typo with the `=` operator from escaping the compiler's notice? Examples:
    ```cpp
    // Wrong
    if (variable == 1);

    // Reason: What if there's a typo?
    //         The compiler will not catch it!
    if (variable = 1); // Oops!

    // Correct
    if (1 == variable);

    // Reason: This time the compiler will catch
    //         the typo.
    if (1 = variable); // Oops!
    ```

## Testing

Volunteer to review code and test new features and bug fixes! Follow these steps:

1. Look for a [new merge request][KDE_Merge], and add a comment that you are volunteering to test it.
2. Build the merge request on your platform.
3. Test the feature/bug fix and try to break it!
4. Provide feedback on test results in the merge request.
5. Wait for updates to the merge request (if needed).
6. Continue testing and providing feedback until you are confident the code is working.
7. Leave a comment with your approval of the merge request.
8. Once at least one other volunteer tester submits their approval, the merge request will be accepted.

## Translations

Please consider [joining the KDE translations team](https://community.kde.org/Get_Involved/translation) to translate your favorite applications into other languages.

## Tech Support

Knowledgeable about Linux or building for MacOS? Know how to fix that one issue others keep running into? Your answering user questions and troubleshooting in the [KDE bug tracker][KDE_Bugs] would be much appreciated!
