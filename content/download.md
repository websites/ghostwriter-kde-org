---
title: Download
menu:
  main:
    weight: 4
commands:
  ubuntu: |
    $ sudo add-apt-repository ppa:wereturtle/ppa
    $ sudo apt update
    $ sudo apt install ghostwriter
  fedora: |
    $ sudo dnf copr enable wereturtle/stable
    $ sudo dnf install ghostwriter
---

+ Windows 10 - available only as [portable 64 bit](https://github.com/KDE/ghostwriter/releases/latest)
+ [Linux repos](#linux)
+ MacOS -[build from source](#macos) (community-supported)

+ [Get the source!](https://invent.kde.org/office/ghostwriter)

## Linux

Enter the following commands based on your Linux distribution.

{{< downloads "ubuntu" "fedora" >}}

## MacOS

MacOS has unofficial support provided by the community.
The app mostly works but for a few quirks.  Unfortunately, you
will have to compile the source in order to run it, as there is
currently no installer available.

Download the source code from the
[GitLab repository](https://invent.kde.org/office/ghostwriter) and see
the README.md file for build instructions.

If you would like to contribute to improving MacOS support,
please see the [contributing guidelines](/contribute).
